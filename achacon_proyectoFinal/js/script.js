function showForm() {
    var emailValue = document.getElementById("emailInput").value;

    if (emailValue!="") {
        document.getElementById("emailFormInput").value = emailValue;
    }

    document.getElementById("subscribeFormId").style.display = "block";
  }

function closeForm() {
    document.getElementById("subscribeFormId").style.display = "none";
}

function subscribe() {
    document.getElementById("subscribeFormId").style.display = "none";
    
    let name = document.getElementById("nameFormInput").value;
    let email = document.getElementById("emailFormInput").value;
    let birthday = document.getElementById("birthdayFormInput").value;
    let identification = document.getElementById("idFormInput").value;
    let nationality = document.getElementById("nationalityFormInput").value;
    
    var objJSON = {
        name: name,
        email: email,
        birthday: birthday,
        identification: identification,
        nationality: nationality
    }

    console.log(objJSON);

    alert("Your Subscription Is Completed.")
}
